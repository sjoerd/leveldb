use leveldb::{Builder, Error};
use tempdir::TempDir;

#[test]
fn basics() {
    let d = TempDir::new("open").expect("tmpdir failed");
    let db = Builder::new(&d)
        .create_if_missing()
        .build()
        .expect("Failed to create db");

    db.put(b"foo", b"bar").expect("put failed");
    let foo = db.get(b"foo").expect("get failed");
    assert_eq!(foo.as_deref(), Some(&b"bar"[..]));

    let nope = db.get(b"nope").expect("get failed");
    assert_eq!(nope, None);
}

#[test]
fn fail_inaccessible() {
    let e = Builder::new(&"/nowhere")
        .create_if_missing()
        .build()
        .expect_err("Unexpected success");

    match e {
        Error::Error(..) => (),
        _ => panic!("Unexpected error: {:?}", e),
    }
}

#[test]
fn fail_missing() {
    let e = Builder::new(&"/nowhere")
        .build()
        .expect_err("Unexpected success");

    match e {
        Error::Error(..) => (),
        _ => panic!("Unexpected error: {:?}", e),
    }
}
