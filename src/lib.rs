use std::path::Path;

mod lowlevel;
pub use lowlevel::Error;
use lowlevel::*;

#[derive(Debug, Clone)]
pub struct Builder<'a> {
    name: &'a Path,
    create_if_missing: bool,
}

impl<'a> Builder<'a> {
    pub fn new<P: AsRef<Path>>(name: &'a P) -> Self {
        Self {
            name: name.as_ref(),
            create_if_missing: false,
        }
    }

    pub fn create_if_missing(mut self) -> Self {
        self.create_if_missing = true;
        self
    }

    pub fn build(self) -> Result<Db, Error> {
        let mut options = LevelDbOptions::new().ok_or(Error::Unknown)?;
        if self.create_if_missing {
            options.create_if_missing();
        }
        LevelDb::new(self.name, options).map(Db)
    }
}

#[derive(Debug)]
pub struct Db(LevelDb);

impl Db {
    pub fn put(&self, key: &[u8], data: &[u8]) -> Result<(), Error> {
        self.0.put(key, data)
    }

    pub fn get(&self, key: &[u8]) -> Result<Option<Box<[u8]>>, Error> {
        self.0.get(key)
    }
}
