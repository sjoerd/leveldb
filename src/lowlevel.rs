use std::{
    ffi::{CStr, CString, NulError},
    os::raw::c_char,
    path::Path,
    ptr::NonNull,
};

use leveldb_sys::{
    leveldb_close, leveldb_free, leveldb_get, leveldb_options_create, leveldb_options_destroy,
    leveldb_options_set_create_if_missing, leveldb_options_t, leveldb_put,
    leveldb_readoptions_create, leveldb_readoptions_destroy, leveldb_t,
    leveldb_writeoptions_create, leveldb_writeoptions_destroy,
};
use thiserror::Error;

pub struct LevelDbOptions(NonNull<leveldb_options_t>);

impl LevelDbOptions {
    pub fn new() -> Option<Self> {
        let options = unsafe { leveldb_options_create() };
        NonNull::new(options).map(Self)
    }

    pub fn as_ptr(&self) -> *mut leveldb_options_t {
        self.0.as_ptr()
    }

    pub fn create_if_missing(&mut self) {
        unsafe { leveldb_options_set_create_if_missing(self.as_ptr(), 42) };
    }
}

impl Drop for LevelDbOptions {
    fn drop(&mut self) {
        unsafe { leveldb_options_destroy(self.0.as_ptr()) }
    }
}

#[derive(Error, Debug)]
pub enum Error {
    #[error("Invalid database name")]
    InvalidName,
    #[error("Database name contains a nul")]
    NullName(#[from] NulError),
    #[error("Leveldb Error: {0}")]
    Error(String),
    #[error("Failure without a cause")]
    Unknown,
}

impl Error {
    unsafe fn from_errptr(errptr: *const c_char) -> Error {
        let error = CStr::from_ptr(errptr).to_string_lossy().into_owned();
        leveldb_free(errptr as _);
        Error::Error(error)
    }
}

#[derive(Debug)]
pub struct LevelDb(NonNull<leveldb_t>);

impl LevelDb {
    pub fn new<N: AsRef<Path>>(name: N, options: LevelDbOptions) -> Result<Self, Error> {
        let name = name.as_ref().to_str().ok_or(Error::InvalidName)?;
        let name = CString::new(name)?;
        let mut errptr = std::ptr::null_mut();

        let db = unsafe { leveldb_sys::leveldb_open(options.as_ptr(), name.as_ptr(), &mut errptr) };

        if let Some(db) = NonNull::new(db) {
            Ok(LevelDb(db))
        } else if errptr.is_null() {
            Err(Error::Unknown)
        } else {
            Err(unsafe { Error::from_errptr(errptr) })
        }
    }

    pub fn put(&self, key: &[u8], data: &[u8]) -> Result<(), Error> {
        let mut errptr = std::ptr::null_mut();

        unsafe {
            let options = leveldb_writeoptions_create();
            if options.is_null() {
                return Err(Error::Unknown);
            }
            leveldb_put(
                self.0.as_ptr(),
                options,
                key.as_ptr() as _,
                key.len(),
                data.as_ptr() as _,
                data.len(),
                &mut errptr,
            );
            leveldb_writeoptions_destroy(options);
        }

        if errptr.is_null() {
            Ok(())
        } else {
            Err(unsafe { Error::from_errptr(errptr) })
        }
    }

    pub fn get(&self, key: &[u8]) -> Result<Option<Box<[u8]>>, Error> {
        let mut errptr = std::ptr::null_mut();
        let mut vallen = 0;

        let val = unsafe {
            let options = leveldb_readoptions_create();
            if options.is_null() {
                return Err(Error::Unknown);
            }
            let val = leveldb_get(
                self.0.as_ptr(),
                options,
                key.as_ptr() as _,
                key.len(),
                &mut vallen,
                &mut errptr,
            );
            leveldb_readoptions_destroy(options);
            val
        };

        if errptr.is_null() {
            if vallen == 0 {
                Ok(None)
            } else {
                let slice = unsafe { std::slice::from_raw_parts(val as *mut u8, vallen) };
                let data = Box::from(slice);
                unsafe { leveldb_free(val as _) };
                Ok(Some(data))
            }
        } else {
            Err(unsafe { Error::from_errptr(errptr) })
        }
    }
}

impl Drop for LevelDb {
    fn drop(&mut self) {
        unsafe { leveldb_close(self.0.as_ptr()) }
    }
}

/// TODO make the test clean up after themselves
#[cfg(test)]
use tempdir::TempDir;

#[test]
fn test_open_and_close() {
    let mut options = LevelDbOptions::new().expect("failed to create options");
    options.create_if_missing();

    let d = TempDir::new("open").expect("Failed to create tempdir");
    let _db = LevelDb::new(d, options).expect("Failed to create db");
}

#[test]
fn test_fail_non_existing() {
    let options = LevelDbOptions::new().expect("failed to create options");
    let e = LevelDb::new("/non-existing", options).expect_err("Unexpected success");

    match e {
        Error::Error(..) => (),
        _ => panic!("Unexpected error: {:?}", e),
    }
}
